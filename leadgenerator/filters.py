"""Filters which use keyword whitelist and blacklist
against CL post result strings.
"""

import re
from config import keywords, filter_words

def are_keywords_present(search_str):
    """Check if any of the whitelisted keywords are present."""

    key_present = False
    for keyword in keywords:
        if re.search(keyword, search_str, re.IGNORECASE) is not None:
            key_present = True
            break
    return key_present


def no_filterwords_present(search_str):
    """Confirm that none of the blacklisted keywords are present."""

    filter_pass = True
    for filterword in filter_words:
        if re.search(filterword, search_str, re.IGNORECASE) is not None:
            filter_pass = False
    return filter_pass