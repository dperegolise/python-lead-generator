"""General purpose Utilities"""

import re

def replace_href_with_full_url(city, search_str):
    if re.search("href=\"http", search_str) is not None:
        return search_str #if link isnt a relative path, return full link
    return re.sub("href=\"", "href=\"http://" + city + ".craigslist.org",
                  search_str)


def get_post_title(search_str):
    return re.search('(?<=hdrlnk">).*?(?=<\s*/\s*a\s*>)', search_str).group(0)


def clean_city(city):
    return city.lower().replace(" ", "").replace(".", "")