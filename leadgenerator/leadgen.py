#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""Lead generator used to scrape latest leads on web-design from craigslist.

This module is meant to be executed as a stand-alone script, to be run on a
timer.
"""

import re
import urllib
import urllib2
from config import *
from filters import *
from utils import *

results = {}


def print_scrape_all_cities():
    """Scrape data from every city.

    This method will search today's posts from every city, and
    filter based on a whitelist and blacklist of keywords.
    Finally, the results will be printed.
    """

    for city in cities:
        city = clean_city(city)
        for keyword in keywords:
            cl_posts = scrape_webpage(city, keyword)
            if cl_posts is not None:
                for match in cl_posts:
                    result = city + " " + match.group(0) + "<br>"
                    result = replace_href_with_full_url(city, result)
                    result_title = get_post_title(result)

                    if are_keywords_present(result) and \
                       no_filterwords_present(result) and \
                       result_title not in results:
                        results[result_title] = result
    print results.values()


def scrape_webpage(city, query):
    """Issue GET request to CL city webpage, and return results.

    Results are extracted from the HTML using a regular expresion.
    """

    params = urllib.urlencode({'sort': 'rel', 'postedToday':
                               1, 'query': query, 'srchType': 'T'})
    # req = urllib2.Request("https://" + city +
    #                       ".craigslist.org/search/ggg", params)
    # keepalive_handler = HTTPHandler()
    # opener = urllib2.build_opener(keepalive_handler)
    # urllib2.install_opener(opener)
    try:
        f = urllib2.urlopen("https://" + city +
                          ".craigslist.org/search/ggg", params)
    except urllib2.HTTPError as e:
        return
    except urllib2.URLError as e:
        return

    cl_search_page = f.read()
    cl_posts = re.finditer('(<time).+?<\s*a\shref.*?hdrlnk">.*?<\s*/\s*a\s*>',
                           cl_search_page, flags=re.S)
    return cl_posts
    f.close()

print_scrape_all_cities()
